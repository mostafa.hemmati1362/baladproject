package com.hemmati.baladproject.enums

enum class UserMode {
    PREMIUM,
    FREE
}