package com.hemmati.baladproject.customView

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import com.hemmati.baladproject.R

class ThreeAngle @JvmOverloads constructor(
    context: Context,
    attr: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attr, defStyleAttr) {

    private val fillPaint = Paint()
    private val path = Path()

    init {
        fillPaint.style = Paint.Style.FILL
        fillPaint.color = ContextCompat.getColor(context, R.color.red)
        fillPaint.isAntiAlias = true
        fillPaint.isDither = true
    }

    override fun onDraw(canvas: Canvas?) {
        val x = this.width
        val y = this.height
        path.moveTo(0f, 0f)

        path.lineTo(0f, 0f)
        path.lineTo((x).toFloat(), 0f)

        path.lineTo(0f, 0f)
        path.lineTo(0f, (y / 1.7).toFloat())

        path.lineTo(0f, (y / 1.7).toFloat())
        path.lineTo((x).toFloat(), 0f)

        path.close()
        canvas?.drawPath(path, fillPaint)
    }

}