package com.hemmati.baladproject.model

import com.hemmati.baladproject.enums.PlayMode

data class Song(
    var duration: Int,
    var playMode: PlayMode,
    var songTitle:String,
    var totalTime:Int
)
