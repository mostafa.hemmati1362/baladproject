package com.hemmati.baladproject.listener

interface ErrorListener {
    fun errorMessage(msg:String)
}