package com.hemmati.baladproject

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hemmati.baladproject.enums.PlayMode
import com.hemmati.baladproject.model.Song
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_header.view.*
import kotlinx.android.synthetic.main.list_item_recyclerview.view.*

class RvAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val list = arrayListOf<Song?>()

    private val firstItemViewType = 0
    private val nextItemViewType = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == firstItemViewType)
            HeaderViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.list_item_header,
                    parent,
                    false
                )
            )
        else
            ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.list_item_recyclerview,
                    parent,
                    false
                )
            )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is HeaderViewHolder) {
            holder.itemView.header.visibility = View.INVISIBLE
        } else if (holder is ViewHolder)
            list[position]?.let { holder.bind(it) }
    }

    override fun getItemCount(): Int = list.size

    override fun getItemViewType(position: Int) = if (list[position] == null)
        firstItemViewType else nextItemViewType

    fun submitList(list: List<Song>) {
        this.list.clear()
        this.list.add(null)
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view)
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(song: Song) {
            val playModeDrawable = when (song.playMode) {
                PlayMode.PlAY -> R.drawable.ic_baseline_play_arrow
                PlayMode.PAUSE -> R.drawable.ic_baseline_pause
                else -> R.drawable.ic_baseline_stop
            }
            itemView.playMode.setBackgroundResource(playModeDrawable);
            itemView.songDuration.text = song.duration.toString()
            itemView.songTitle.text = song.songTitle

        }
    }
}