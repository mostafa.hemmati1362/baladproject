package com.hemmati.baladproject

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.hemmati.baladproject.enums.PlayMode
import com.hemmati.baladproject.model.Song
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupRecyclerView()

    }


    private fun getSongList(): ArrayList<Song> {
        return arrayListOf(
            Song(50, PlayMode.PAUSE, "1", 100),
            Song(19, PlayMode.PlAY, "2", 30),
            Song(20, PlayMode.STOP, "3", 25),
            Song(0, PlayMode.STOP, "4", 20),
            Song(18, PlayMode.STOP, "5", 22),
            Song(0, PlayMode.STOP, "6", 22),
            Song(0, PlayMode.STOP, "7", 22),
            Song(0, PlayMode.STOP, "8", 22),
            Song(0, PlayMode.STOP, "9", 22),
            Song(0, PlayMode.STOP, "10", 22),
            Song(0, PlayMode.STOP, "11", 22),
            Song(0, PlayMode.STOP, "12", 22),
            Song(0, PlayMode.STOP, "13", 22),
            Song(0, PlayMode.STOP, "14", 22),
            Song(0, PlayMode.STOP, "15", 22),
            Song(0, PlayMode.STOP, "16", 22)
        )
    }

    private fun setupRecyclerView() {
        val adapter = RvAdapter()
        recyclerView.adapter = adapter
        adapter.submitList(
            getSongList()
        )
    }

}