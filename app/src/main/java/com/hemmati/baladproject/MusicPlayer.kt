package com.hemmati.baladproject

import android.content.Context
import android.util.Log
import com.hemmati.baladproject.enums.PlayMode
import com.hemmati.baladproject.enums.UserMode
import com.hemmati.baladproject.listener.ErrorListener
import com.hemmati.baladproject.model.Song

class MusicPlayer constructor(private val songList: ArrayList<Song>, private val context: Context) :
    ErrorListener {
    private var userMode: UserMode = UserMode.FREE
    private var index: Int = 0

    fun setUserMode(userMode: UserMode) {
        this.userMode = userMode
    }

    fun getCurrentSong(): Song = songList[index]

    fun nextSongList(): List<Song> = songList.subList(index, songList.size - 1)

    fun replacePlayList(newSongList: ArrayList<Song>) {
        when (userMode) {
            UserMode.PREMIUM -> {
                clearOldList()
                songList.addAll(newSongList)
                getNextSong()
            }
            UserMode.FREE -> {
                if (newSongList.size > 5) {
                    clearOldList()
                    songList.addAll(newSongList.shuffled())
                    getNextSong()
                } else errorMessage(context.getString(R.string.freeUserListMsg))
            }
        }
    }

    private fun clearOldList() {
        val temp = songList.filterIndexed { index, _ ->
            index < this.index
        }
        songList.clear()
        songList.addAll(temp)
    }

    fun pauseCurrentSong(duration: Int) {
        if (songList[index].playMode == PlayMode.PlAY) {
            songList[index].duration = duration
            songList[index].playMode = PlayMode.PAUSE
        }
    }

    fun replaySong(): Int? {
        if (songList[index].playMode == PlayMode.PAUSE) {
            songList[index].playMode = PlayMode.PlAY
            return songList[index].duration
        }
        return null

    }

    fun stopCurrentSong() {
        songList[index].playMode = PlayMode.STOP
        songList[index].duration = 0
    }

    fun getNextSong(): Song? {
        if (index < songList.size - 1) {
            songList[index].playMode = PlayMode.STOP
            index++
            songList[index].playMode = PlayMode.PlAY
            return songList[index]
        } else errorMessage(context.getString(R.string.lastItem))
        return null
    }

    fun getPreviousSong(): Song? {
        if (userMode == UserMode.PREMIUM) {
            if (index > 0) {
                songList[index].playMode = PlayMode.STOP
                index--
                songList[index].playMode = PlayMode.PlAY
                return songList[index]
            } else errorMessage(context.getString(R.string.firtItem))
        } else errorMessage(context.getString(R.string.urNotPremium))
        return null
    }

    fun setNextSong(song: Song) {
        if (userMode == UserMode.PREMIUM)
            songList[index + 1] = song
        else errorMessage(context.getString(R.string.urNotPremium))
    }


    override fun errorMessage(msg: String) {
        Log.e(TAG, "errorMessage: $msg")
    }

    companion object {
        private const val TAG = "MusicPlayer"
    }

}
